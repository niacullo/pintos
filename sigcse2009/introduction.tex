\section{Introduction}
\label{sec:intro}

Despite the wide use of higher-level languages and environments, gaining a robust
understanding of operating systems (OS) fundamentals and training in the current design and
implementation practices of OS remains a cornerstone of 
undergraduate computer science education.

% abstract/concrete
% internal/external
Approaches to teaching OS courses generally fall along two axes: 
whether the treatment of the material is abstract or 
concrete~\cite{Hovemeyer2004Running}, and whether they adopt an
internal or external perspective~\cite{Deitel2003Operating}.
An abstract approach discusses algorithms and techniques used in operating 
systems and may include partial implementation or simulation exercises,
whereas a concrete approach stresses the design and creation of 
realistic artifacts.
When adopting the internal perspective, an operating system is considered
from the point of view of the OS designer, whereas the external perspective 
assumes the role of a user or programmer using an OS's 
facilities~\cite{Bryant2002Computer}.

% we don't really say about the benefits of the 'internal' approach here.
% is this too controversial for this audience?
The approach advocated in this paper is concrete and adopts the internal perspective.  
Students who have studied, implemented, and evaluated core OS techniques attain 
a deeper understanding than those who have merely studied them.
Finally, adopting a concrete approach brings significant secondary
benefits, including training in modern software development techniques
and tools.  The C language remains the implementation language of choice
for operating system kernels and for many embedded systems.
Practice and debugging experience in C, particularly using modern tools,
not only increases students' ``market value,''~\cite{1292450} but provides students with
the insight that a low-level programming and runtime model is not incompatible
with high-level tools.

Designing course material for the internal and concrete 
approach is challenging.  While realistic, 
assignments should be relatively simple and doable within a realistic time frame.  
Whereas assignments should use current hardware architectures, 
they must not impart too much transient knowledge.
Assignments should include and emphasize the use of modern software 
engineering practices and tools, such as dynamic program analysis.

This paper introduces Pintos, an instructional operating system kernel that 
has been in use at multiple institutions for about 4 years.  Pintos provides 
a bootable kernel for standard x86-based personal computers.  We provide four
structured assignments in which students implement a basic priority
scheduler, a multi-level feedback queue scheduler, a process-based 
multi-programming system, page-based virtual memory
including on-demand paging, memory-mapped files, and swapping, and a
simple hierarchical file system.  An overview of the projects enabled
by Pintos is given in Figure~\ref{fig:pintosdetail}, which shows which
software is provided as support code and test cases,
the parts that are created by students, and their relationship. 

Although Pintos follows in the tradition of instructional operating systems 
such as Nachos~\cite{Christopher1993Nachos}, OS/161~\cite{Holland2002New}, and
GeekOS~\cite{Hovemeyer2004Running}, 
PortOS~\cite{Atkin2002PortOS},
BLITZ~\cite{PorterOverview},
JOS~\cite{1088822}, or Yalnix~\cite{1088822},
we believe that it is unique in two
aspects.  First, Pintos runs on both real hardware and in emulated and
simulated environments.\footnote{\small GeekOS is the only other system that claims to 
also run on real PC hardware; it requires, however, a dedicated disk and 
does not support running off USB devices, making it impractical for many 
laboratory settings.}  We believe that having the ability to see the outcome
of their project interact with actual hardware increases student engagement.
Second, we have created a set of analysis tools
for the emulated environment that allows students to detect programming
mistakes such as race conditions.  Figure~\ref{fig:pintosenvs} shows
the three environments in which the same kernel can be run.

Others have used Linux, either on dedicated devices (e.g., iPodLinux~\cite{1352199}),
or in virtualized environments~\cite{1008027,1352648,Nieh2005Experiences}, to provide
an internal, concrete perspective.  Compared to those approaches, Pintos provides
a similar level of realism in that students can see the results of their
work on concrete or virtualized hardware, but does not require that students 
understand the often arcane and ill-documented interfaces of the Linux kernel,
which were not designed from an educational perspective.  
By contrast, all Pintos code is written to be studied by students.

%This paper summarizes the design philosophy that underlies Pintos,
%details its structure, and outlines the nature and learning goals of each
%assignment.

\pintosenvfigure{}

\pintosdetailfiguretwo{}

% Challenges.
% How to embed principles?
% How to teach software engineering?
% Realism vs. Simplification

%User-Mode Linux\cite{1008027}
%Virtualization\cite{1352648}
%Linux in VM\cite{Nieh2005Experiences}

